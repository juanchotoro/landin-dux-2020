(function(angular, appName) {
    'use strict';
    var app = angular.module(window.appName);

    function routingConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state({
                name: 'inicio',
                url: '/',
                templateUrl: 'views/inicio.html',
                controller: 'inicio-controller'
            })
            .state({
                name: 'thankyou',
                url: '/thankyou',
                templateUrl: 'views/thankyou.html'
            })

        $urlRouterProvider.otherwise('/');

    }
    routingConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    app.config(routingConfig);

})(window.angular, window.appName);