(function(angular) {
    'use strict';
    if (!angular) { throw 'Angular no esta definido'; }

    window.appName = "landing-dux";

    var config = function() {

        $('.carousel').carousel({
            interval: 15000
        });

    }
    config.$inject = [];

    var run = function($rootScope) {
        angular.element('#disclaimer').modal('show')
            //Agregar clase al body o al footer en determinadas rutas

    }

    run.$inject = ['$rootScope'];

    angular.module(window.appName, ['ui.router'])
        .config(config)
        .run(run);

})(window.angular);