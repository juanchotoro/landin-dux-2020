var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch'),
    server = require('gulp-server-livereload'),
    less = require('gulp-less'),
    clean = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer');

var rutas = {
    js: {
        origen: 'app/js/src/**/*.js',
        destino: 'app/js/'
    },
    css: {
        origen: 'app/css/src/**/*.less',
        destino: 'app/css/'
    }
};
var bower_path = {
    css: [
        "bower_components/bootstrap/dist/css/bootstrap.min.css",
        "bower_components/angularjs-datepicker/dist/angular-datepicker.css"
    ],
    js: [
        "bower_components/jquery/dist/jquery.min.js",
        "bower_components/bootstrap/dist/js/bootstrap.min.js",
        "bower_components/angular/angular.min.js",
        "bower_components/angular-ui-router/release/angular-ui-router.min.js",
        "bower_components/firebase/firebase.js",
        "bower_components/angularfire/dist/angularfire.min.js",
        "bower_components/angularjs-datepicker/dist/angular-datepicker.min.js"
    ]
};

gulp.task('js', function() {
    gulp.src(rutas.js.origen)
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(rutas.js.destino));
});

gulp.task('css', function() {
    gulp.src(rutas.css.origen)
        .pipe(concat('styles.min.css'))
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(clean())
        .pipe(gulp.dest(rutas.css.destino));
});

gulp.task('js_vendor', function() {
    gulp.src(bower_path.js)
        .pipe(concat('vendor.min.js'))
        //.pipe(uglify())
        .pipe(gulp.dest(rutas.js.destino));
});

gulp.task('css_vendor', function() {
    gulp.src(bower_path.css)
        .pipe(concat('vendor.min.css'))
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(clean())
        .pipe(gulp.dest(rutas.css.destino));
});

gulp.task('watch', ['css_vendor', 'js_vendor', 'js', 'css'], function() {
    gulp.watch(rutas.js.origen, ['js']);
    gulp.watch(rutas.css.origen, ['css']);
});

gulp.task('server', function() {
    gulp.src('app')
        .pipe(server({
            port: 3000,
            livereload: true,
            open: true
        }));
});

gulp.task('default', ['watch', 'server']);